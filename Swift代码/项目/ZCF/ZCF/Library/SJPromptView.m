//
//  SJPromptView.m
//  Init
//
//  Created by 赵世杰 on 16/3/24.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "SJPromptView.h"

@interface SJPromptView ()

@property(nonatomic,strong)UILabel *label ;

@property(nonatomic,copy)id superView ;

@end


@implementation SJPromptView


- (void)addToWithController:(id)target withFrame:(CGRect)frame withTime:(float)time
{
    self.frame = frame ;
    self.backgroundColor = [UIColor colorWithRed:0.3316 green:0.3316 blue:0.3316 alpha:1.0] ;
    self.layer.masksToBounds = YES ;
    self.layer.cornerRadius = 8 ;
    if([target isKindOfClass:[UIViewController class]])
    {
        __weak UIViewController *VC = target ;
        [VC.view addSubview:self] ;
    }
    else
    {
        __weak UIViewController *VC = [self viewController:target] ;
        [VC.view addSubview:self] ;
    }

    _label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)] ;
    _label.backgroundColor = [UIColor clearColor] ;
    _label.textColor = [UIColor whiteColor] ;
    _label.text = _textString ;
    _label.textAlignment = NSTextAlignmentCenter ;
    [self addSubview:_label] ;
    dispatch_sync(dispatch_get_global_queue(0, 0), ^{
        [UIView animateWithDuration:time animations:^{
            self.alpha = 0 ;
        } completion:^(BOOL finished) {
            [self removeFromSuperview] ;
        }];
    });
    
}


- (UIViewController *)viewController:(UIView *)view
{

    UIResponder *responder = view;
    while ((responder = [responder nextResponder]))
    {
        if ([responder isKindOfClass: [UIViewController class]])
        {
            return (UIViewController *)responder;
        }
    }
    return nil;
}

@end
