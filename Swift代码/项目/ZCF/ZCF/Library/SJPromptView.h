//
//  SJPromptView.h
//  Init
//
//  Created by 赵世杰 on 16/3/24.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SJPromptView : UIView

@property(nonatomic,strong)NSString *textString ;



- (void)addToWithController:(nonnull id)target withFrame:(CGRect)frame withTime:(float)time ;

@end
