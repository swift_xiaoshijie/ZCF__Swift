//
//  BaseModel.h
//  WXMovie
//
//  Created by 朱思明 on 15/8/28.
//  Copyright (c) 2015年 朱思明. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject


// 自定义初始化方法（带有自动映射属性的作用）
- (instancetype)initWithContentDic:(NSDictionary *)dic;

// 获取当前字典的映射关系
- (NSDictionary *)keyAndAttWithContentDic:(NSDictionary *)dic;
@end
