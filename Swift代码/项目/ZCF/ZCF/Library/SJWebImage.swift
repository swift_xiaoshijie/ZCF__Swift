//
//  SJWebImage.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/21.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit




// MARK: - 类别扩展方法
extension UIImageView{
    
    /**
     异步加载图片的方法
     
     - parameter urlString: 图片加载的网络链接
     */
    func SJ_setImageWithUrlString(_ urlString:String)
    {
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async { () -> Void in
            let url = URL(string: urlString)
            let data = try? Data(contentsOf: url!)
            let image = UIImage(data: data!)
            DispatchQueue.main.async(execute: { () -> Void in
                self.image = image
            })
        }
    }
    
}


