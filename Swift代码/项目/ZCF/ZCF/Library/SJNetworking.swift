//
//  SJNetworking.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/18.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit


/*
class SJNetworking: NSObject {
    
    var result:AnyObject!
    
    
    /**
     POST请求
     
     - parameter urlString:   请求地址
     - parameter HttpHeader:  请求头信息
     - parameter parameters:  请求体配置信息
     - parameter resultBlock: 请求成功返回数据
     - parameter errorBlock:  请求失败调用
     */
    func POSTRequest(urlString: String , HttpHeader:[String:String]? = nil , parameters: [String:String]? = nil,resultBlock:(result:(AnyObject))->Void,errorBlock:()->Void)
    {
        // 拼接链接
        var urlStr = urlString + "?"
        // 判断请求参数是否存在
        if((parameters) != nil)
        {
            for (paramKey , paramValue) in parameters!
            {
                urlStr = urlStr + paramKey + "=" + paramValue
            }
        }
        // 创建请求对象，并设置
        let url = NSURL(string: urlString)
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "POST"
        request.timeoutInterval = 20
        // 判断请求头是否存在
        if((HttpHeader) != nil)
        {
            for (HTTPHeaderKey , HTTPHeaderValue) in HttpHeader!
            {
                request .setValue(HTTPHeaderValue, forHTTPHeaderField: HTTPHeaderKey)
            }
        }
        // 开发请求并且进行JSON数据我转换和返回
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response:NSURLResponse?, data:NSData?, error:NSError?) -> Void in
            do
            {
                self.result = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                resultBlock(result: self.result)
            }
            catch
            {
                errorBlock()
            }
        }
    }
    

}

*/
