//
//  CommonTool.m
//  ZCF
//
//  Created by 赵世杰 on 16/4/27.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "CommonTool.h"
//MD5
#import <CommonCrypto/CommonDigest.h>

@implementation CommonTool

/**
 *  md5加密
 *
 *  @param str 密码
 *
 *  @return 加密之后的字符串
 */
+ (NSString *)md5:(NSString *)str
{
    NSLog(@"MD5");
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (int)strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}



/**
 *  字符串转换成颜色
 *
 *  @param color 颜色对应的字符串
 *
 *  @return 字符串转换成的颜色
 */
+ (UIColor *) colorWithHexString: (NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //r
    NSString *rString = [cString substringWithRange:range];
    
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}


/**
 *  颜色转化成图片
 *
 *  @param color 颜色
 *
 *  @return 图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    return image;
}


/**
 *  获取验证码图片
 *
 *  @param urlString 请求地址
 *
 *  @return 图片
 */
+ (NSDictionary *)getImageCodeWithUrl:(NSString *)urlString
{
//    _randomNumber_20 = [self getMylastFiveTime];
    NSString *getRandomNumber_20 = [NSString stringWithFormat:@""];
    for (int i = 0; i < 15; i++) {
        UInt64 recordTime = [[NSDate date] timeIntervalSince1970]*1000 *1000 *1000;
        NSString *timeString = [NSString stringWithFormat:@"%lld", recordTime];
        //    NSLog(@"%@", timeString);
        NSString *subString  = [timeString substringFromIndex:(timeString.length - 5)];
        //        NSLog(@"%@", subString);
        //        NSLog(@"%d", arc4random() % 10);
        getRandomNumber_20 = [NSString stringWithFormat:@"%@%u", getRandomNumber_20,arc4random() % 10];
        if(i == 14){
            getRandomNumber_20 = [NSString stringWithFormat:@"%@%@", subString,getRandomNumber_20];
            //            NSLog(@"i = %d, getRandomNumber = %@", i, getRandomNumber_20);
        }
    }

    NSString *stringUrl = [NSString stringWithFormat:@"%@/jcaptcha?ran=%@", urlString,getRandomNumber_20];
    //        NSString *utf8 = [stringUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    NSLog(@"myNumber = %@ , string = %@",[self getMylastFiveTime] ,stringUrl);
    NSURL *url = [NSURL URLWithString:stringUrl];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *image = [UIImage imageWithData:data];
    NSDictionary *resultDic = @{@"image":image,@"number":getRandomNumber_20} ;
    return resultDic ;
}



@end
