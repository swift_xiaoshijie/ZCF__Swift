//
//  CommonTool.h
//  ZCF
//
//  Created by 赵世杰 on 16/4/27.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonTool : NSObject


/**
 *  md5加密
 *
 *  @param str 密码
 *
 *  @return 加密之后的字符串
 */
+ (NSString *)md5:(NSString *)str ;


/**
 *  字符串转换成颜色
 *
 *  @param color 颜色对应的字符串
 *
 *  @return 字符串转换成的颜色
 */
+ (UIColor *) colorWithHexString: (NSString *)color ;


/**
 *  颜色转化成图片
 *
 *  @param color 颜色
 *
 *  @return 图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color ;


/**
 *  获取验证码图片
 *
 *  @param urlString 请求地址
 *
 *  @return 图片
 */
+ (NSDictionary *)getImageCodeWithUrl:(NSString *)urlString ;



@end
