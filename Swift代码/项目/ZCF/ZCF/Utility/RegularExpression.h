//
//  RegularExpression.h
//  ZCF
//
//  Created by 赵世杰 on 16/4/19.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RegularExpression : NSObject


/**
 *  判断手机号码是否有效
 *
 *  @param mobileNum 手机号码
 *
 *  @return 判断结果
 */
+ (BOOL)isMobileNumber:(NSString *)mobileNum ;




/**
 *  判断设置的密码是否有效
 *
 *  @param textString 密码
 *
 *  @return 判断结果
 */
+ (BOOL)isPasswordTure:(NSString *) textString ;


@end
