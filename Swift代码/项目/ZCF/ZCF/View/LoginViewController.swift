//
//  LoginViewController.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/19.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    
    
    // 控件
    
    // 用户名
    @IBOutlet var userTextField: UITextField!
    // 密码
    @IBOutlet var passwordTextField: UITextField!
    // 显示密码
    @IBOutlet var showPasswordButton: UIButton!
    // 记住密码
    @IBOutlet var rememberPasswordButton: UIButton!
    // 登陆
    @IBOutlet var loginButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // 创建视图
        self._initViews()

    }
    

    
    // 创建视图
    func _initViews()
    {
        // 设置导航栏
        self.createNavig()
        
    }
    
    // 设置导航栏
    func createNavig()
    {
        // 修改背景图片
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "denglu_01"), for: .default)
        // 设置背景标题文字
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        titleLabel.text = "用户登录"
        titleLabel.font = UIFont.systemFont(ofSize: 15)
        titleLabel.textColor = UIColor.white
        titleLabel.textAlignment = .center
        self.navigationItem.titleView = titleLabel
        // 设置背景返回按钮
        let returnButton = UIButton(type: .custom)
        returnButton.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        returnButton.setTitle("取消", for: UIControlState())
        returnButton.setTitleColor(UIColor.white, for: UIControlState())
        returnButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        returnButton.addTarget(self, action: #selector(LoginViewController.returnButtonAction), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: returnButton)
    }
    
    
    //MARK: - 网络请求
    // 网络请求判断密码和账号是否匹配
    func requestLogin()
    {
        let urlString = HttpUrl + "/login"
        let requestHeader:[String:String] = ["version":"2.0","terminal":"ios"]
        let paramDic:[String:String] = ["mobile":self.userTextField.text!,"password":(CommonTool.md5(self.passwordTextField.text) as String).uppercased()]
        let request = HTTPRequest()
        request.POSTRequest(urlString, httpHeader: requestHeader, parameters: paramDic, resultBlock: { (result) -> Void in
            //            print("请求成功")
            //            print(result)
            let code = result["code"] as? NSNumber
            if code == 200
            {
                //                print("登录成功")
                let data:[String:AnyObject] = result["data"] as! Dictionary
                let userSingle:UserSingle = UserSingle.instance ;
                userSingle.token = String(describing: data["token"])
                userSingle.userId =  String(describing: data["userId"])
                //                print(userSingle.token)
                //                print(userSingle.userId)
                
                self.dismiss(animated: true, completion: nil)
            }
            
            }) { () -> Void in
                print("请求失败")
        }
    }

    
    
    //MARK: - 响应事件
    
    // 返回按钮
    func returnButtonAction()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    // 显示密码
    @IBAction func rememberPasswordAction(_ sender: AnyObject) {
    }
    // 记住密码
    @IBAction func showPasswordAction(_ sender: AnyObject) {
    }
    // 登陆
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
       if !RegularExpression.isMobileNumber(self.userTextField.text)
       {
        //            print("号码错误")
            let promptView = SJPromptView()
            promptView.textString = "账号格式有误"
            let frame = CGRect(x: (KScreenWidth-80)/2.0, y: 450*KScreenWidth/320, width: 120, height: 40)
            promptView .addTo(withController: self, withFrame: frame, withTime: 2)
            return
       }
        else if self.passwordTextField.text!.isEmpty
       {
            let promptView = SJPromptView()
            promptView.textString = "请输入密码"
            let frame = CGRect(x: (KScreenWidth-80)/2.0, y: 450*KScreenWidth/320, width: 100, height: 40)
            promptView.addTo(withController: self, withFrame: frame, withTime: 2)
            return
        }
        
        // 登录请求
        self.requestLogin()

    }
    // 注册
    @IBAction func registerButtonAction(_ sender: AnyObject) {
        let registerVC = RegisterViewController()
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
    
}
