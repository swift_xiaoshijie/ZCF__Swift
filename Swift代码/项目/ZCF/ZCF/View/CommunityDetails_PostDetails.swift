//
//  CommunityDetails_PDVC.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/22.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class CommunityDetails_PostDetails: BaseModel {
    var content:String!
    var createTime:String!
    var imageList:Array<AnyObject>!
    var likes:NSNumber!
    var replyNum:NSNumber!
    var terminal:String!
    var title:String!
    var userHeaderImg:String!
    var userLikeStatus:NSNumber!
    var userNickname:String!
    var views:NSNumber!
}
