//
//  ItemsShowCCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/14.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ItemsShowCCell: UICollectionViewCell {
    
    
    // 数据
    var ISModel_Mall:ItemShow_Mall!
    /// 变量
    
    // 图片
    @IBOutlet var itemImage: UIImageView!
    // 名称
    @IBOutlet var itemNameLabel: UILabel!
    // 积分数字
    @IBOutlet var itemIntegralLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    // 复用的方法
    override func layoutSubviews() {
        super.layoutSubviews()
        
 /// 设置数据
        // 加载图片
        var goodsImageList:Array<AnyObject> = ISModel_Mall.goodsImageList
        var dic:[String:AnyObject] = goodsImageList[0] as! Dictionary
        let url = URL(string:dic["url"] as! String)
        self.itemImage .sd_setImage(with: url)
        // 显示文字
        self.itemNameLabel.text = self.ISModel_Mall.name
        self.itemIntegralLabel.text = String(describing: self.ISModel_Mall.score)
        
    }

}
