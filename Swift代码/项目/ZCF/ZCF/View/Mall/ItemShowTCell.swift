//
//  ItemShowTCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/14.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ItemShowTCell: UITableViewCell {
    
    
    // 数据model对象
    var itemShow_Mall:ItemShow_Mall!
    
    
    @IBOutlet var headImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var integralLabel: UILabel!
    @IBOutlet var numberLabel: UILabel!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = GrayColorInit
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // 设置数据
        // 设置图片
        let goodsImageList:Array = self.itemShow_Mall.goodsImageList
        let dic:[String:AnyObject] = goodsImageList.first as! Dictionary
        let url = URL(string: dic["url"] as! String)
        self.headImageView .sd_setImage(with: url)
        
        // 设置文本
        self.nameLabel.text = self.itemShow_Mall.name
        self.integralLabel.text = String(describing: self.itemShow_Mall.score)
        self.numberLabel.text = String(describing: self.itemShow_Mall.exchangeNum)
    }
    
}
