//
//  CellModel_community.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/11.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class CellModel_community: BaseModel {
    
    var content:String!
    var createTime:String!
    var id:NSNumber!
    var imageList:Array<AnyObject>!
    var likes:NSNumber!
    var position:String!
    var replyNum:Int!
    var stick:NSNumber!
    var terminal:String!
    var title:String!
    var userHeaderImg:String!
    var userLikeStatus:Int!
    var userNickname:String!
    var views:NSNumber!
    
}
