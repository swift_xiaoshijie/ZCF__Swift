//
//  ShareContentTableViewCell2.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/12.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ShareContentTableViewCell2: UITableViewCell {
    
    
    
    // model数据
    var cellModel_COM:CellModel_community!
    
    
    /// 控件
    @IBOutlet var bgView: UIView!
    // 头像视图
    @IBOutlet var headImageView: UIImageView!
    // 名字视图
    @IBOutlet var nameLabel: UILabel!
    // 发布时间视图
    @IBOutlet var timeLabel: UILabel!
    // 发布标题
    @IBOutlet var titleLabel: UILabel!
    // 发布内容
    @IBOutlet var contentLabel: UILabel!
    // 发布图片
    @IBOutlet var imageView1: UIImageView!
    @IBOutlet var imageView2: UIImageView!
    @IBOutlet var imageView3: UIImageView!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // 创建视图
        self._initViews()
    }
    
    
    //MARK: - 创建视图
    func _initViews()
    {
        
        self.contentView.backgroundColor = UIColor ( red: 0.9137, green: 0.9042, blue: 0.9232, alpha: 1.0 )
        
        self.bgView.layer.cornerRadius = 4
        self.bgView.layer.masksToBounds = true
        
        self.headImageView.layer.cornerRadius = self.headImageView.width/CGFloat(2.0)
        self.headImageView.layer.masksToBounds = true
    }

    //MARK: - 复用的方法
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
        // 设置头像
        let headImageUrl = URL(string: self.cellModel_COM.userHeaderImg)
        self.headImageView .sd_setImage(with: headImageUrl)
        // 设置名字
        self.nameLabel.text = self.cellModel_COM.userNickname
        // 设置发布时间
        self.timeLabel.text = self.cellModel_COM.createTime
        // 设置发布的标题 
        self.titleLabel.text = self.cellModel_COM.title
        // 设置发布的内容
        self.contentLabel.text = self.cellModel_COM.content
        
        
        var imageViewAarray = [self.imageView1,self.imageView2,self.imageView3]
        
        for j in 0  ..< imageViewAarray.count 
        {
            imageViewAarray[j]?.isHidden = true
        }
        for i in 0  ..< self.cellModel_COM.imageList.count 
        {
            
            var dic:[String:String] = self.cellModel_COM.imageList[i] as! Dictionary
            let url = URL(string: dic["url"]! as String)
            imageViewAarray[i]? .sd_setImage(with: url)
            imageViewAarray[i]?.isHidden = false
            if i == 2
            {
                break
            }
        }
        
    }
    
}
