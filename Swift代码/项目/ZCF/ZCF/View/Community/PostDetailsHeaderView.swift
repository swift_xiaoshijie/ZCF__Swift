
//
//  PostDetailsHeaderView.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/25.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class PostDetailsHeaderView: UIView {

    /// 数据
    var postDetails_model:CommunityDetails_PostDetails!
    var imageViewArray:Array<UIImageView>!
    
    
    /// 控件
    
    // 头像
    @IBOutlet var headerImageView: UIImageView!
    // 名称
    @IBOutlet var nameLabel: UILabel!
    // 发布时间
    @IBOutlet var timeLabel: UILabel!
    // 标题
    @IBOutlet var titleLabel: UILabel!
    // 内容
    @IBOutlet var contenLabel: UILabel!
    
    // 9张图片
    @IBOutlet var imageView1: UIImageView!
    @IBOutlet var imageView2: UIImageView!
    @IBOutlet var imageView3: UIImageView!
    @IBOutlet var imageView4: UIImageView!
    @IBOutlet var imageView5: UIImageView!
    @IBOutlet var imageView6: UIImageView!
    @IBOutlet var imageView7: UIImageView!
    @IBOutlet var imageView8: UIImageView!
    @IBOutlet var imageView9: UIImageView!
    
    // 浏览量，点赞，评论
    @IBOutlet var lookButton: UIButton!
    @IBOutlet var zanButton: UIButton!
    @IBOutlet var commentButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.headerImageView.layer.masksToBounds = true
        self.headerImageView.layer.cornerRadius = self.headerImageView.height/2.0
        self.imageViewArray = [imageView1,imageView2,imageView3,imageView4,imageView5,imageView6,imageView7,imageView8,imageView9]
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // 三个button
        let buttonArray:Array<UIButton> = [self.lookButton , self.zanButton , self.commentButton]
        // 三个button上面的图片
        let buttonImageArray:Array<String> = ["sq_25","sq_22","sq_28"]
        // 三个文本
        let buttonTitleArray:Array<String> = [String(describing: self.postDetails_model.views) , String(describing: self.postDetails_model.likes) , String(describing: self.postDetails_model.replyNum)]
        
        // 添加浏览量，点赞，评论上面的图标和文字
        for i in 0  ..< 3 
        {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            imageView.center = CGPoint(x: buttonArray[i].center.x-10, y: buttonArray[i].center.y)
            imageView.image = UIImage(named: buttonImageArray[i])
            self.addSubview(imageView)
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
            label.center = CGPoint(x: buttonArray[i].center.x+20, y: buttonArray[i].center.y)
            label.textAlignment = .left
            label.textColor = UIColor.lightGray
            label.font = UIFont.systemFont(ofSize: 12)
            label.tag = 1000 + i
            label.text = buttonTitleArray[i]
            self.addSubview(label)
        }

       
        // 设置基本信息
        self.headerImageView .sd_setImage(with: URL(string: self.postDetails_model.userHeaderImg))
        self.nameLabel.text = self.postDetails_model.userNickname
        self.timeLabel.text = self.postDetails_model.createTime
        self.titleLabel.text = self.postDetails_model.title
        self.contenLabel.text = self.postDetails_model.content
        // 隐藏所有的图片
        for i in 0  ..< 9 
        {
            self.imageViewArray[i].isHidden = true
        }
        // 显示需要加载的图片并设置
        for i in 0  ..< self.postDetails_model.imageList.count 
        {
            if i >= 9
            {
                return
            }
            self.imageViewArray[i].isHidden = false
            let imageDic:[String:AnyObject] = self.postDetails_model.imageList[i] as! Dictionary
            let urlString = imageDic["detailUrl"] as! String
            self.imageViewArray[i] .sd_setImage(with: URL(string: urlString))
        }
    }

}
