//
//  ShareContentTableViewCell1.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/11.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ShareContentTableViewCell1: UITableViewCell {
    
    
    
    // cell的数据Model
    var cellModel_COM:CellModel_community!
    
    /// 控件


    @IBOutlet var bgView: UIView!
    // 头像视图
    @IBOutlet var headImageView: UIImageView!
    // 名称视图
    @IBOutlet var nameLabel: UILabel!
    // 发布时间视图
    @IBOutlet var timeLabel: UILabel!
    // 发布标题视图
    @IBOutlet var titleLabel: UILabel!
    // 发布内容视图
    @IBOutlet var contentLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // 创建控件
        self._initViews()
    }
    
    
    //MARK: - 创建控件
    func _initViews()
    {
        
        self.contentView.backgroundColor = UIColor ( red: 0.9137, green: 0.9042, blue: 0.9232, alpha: 1.0 )
        
        self.bgView.layer.cornerRadius = 4
        self.bgView.layer.masksToBounds = true
        
        self.headImageView.layer.cornerRadius = self.headImageView.width/CGFloat(2.0)
        self.headImageView.layer.masksToBounds = true
    }
    
    //MARK: - 复用方法
    override func layoutSubviews() {
        super.layoutSubviews()
        // 设置头像
        let url = URL(string: self.cellModel_COM.userHeaderImg)
        self.headImageView .sd_setImage(with: url)
        // 设置名称
        self.nameLabel.text = self.cellModel_COM.userNickname
        // 设置发布时间
        self.timeLabel.text = self.cellModel_COM.createTime
        // 设置发布标题
        self.titleLabel.text = self.cellModel_COM.title
        // 设置发布内容
        self.contentLabel.text = self.cellModel_COM.content
        
    }

    
}
