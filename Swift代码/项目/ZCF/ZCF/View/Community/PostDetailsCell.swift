//
//  PostDetailsCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/28.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class PostDetailsCell: UITableViewCell {
    
    
    // 数据model
    var commentModel_community:CommentModel_community!
    
    // 头像
    @IBOutlet var headImageView: UIImageView!
    // 用户名
    @IBOutlet var nameLabel: UILabel!
    // 评论时间
    @IBOutlet var timeLabel: UILabel!
    // 评论内容
    @IBOutlet var contentLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.headImageView.layer.cornerRadius = self.headImageView.height/2.0
        self.headImageView.layer.masksToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        // 设置内容
        self.headImageView .sd_setImage(with: URL(string: self.commentModel_community.userHeaderImg))
        self.nameLabel.text = self.commentModel_community.userNickname
        self.timeLabel.text = self.commentModel_community.createTime
        self.contentLabel.text = self.commentModel_community.content
    }
    
}
