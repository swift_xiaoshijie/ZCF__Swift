//
//  DisPlayTCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/15.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class DisPlayTCell: UITableViewCell {
    
    
    // 传进来的数据
    var titleString:String!

    // 控件
    @IBOutlet var stringLabel: UILabel!
    // 标示线
    @IBOutlet var markerLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
    }

    
    // 复用方法
    override func layoutSubviews() {
        super.layoutSubviews()
    
        self.stringLabel.text = self.titleString
    }
    
}
