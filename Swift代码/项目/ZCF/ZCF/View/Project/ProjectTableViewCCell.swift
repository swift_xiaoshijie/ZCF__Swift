//
//  ProjectTableViewCCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/15.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ProjectTableViewCCell: UITableViewCell {
    
    
    //MARK: - model数据
    var projectModel_Pro:ProjectListModel_Project!
    
    //MARK: - 控件
    @IBOutlet var projectImageView: UIImageView!
    @IBOutlet var moneyLabel: UILabel!
    @IBOutlet var targetLabel: UILabel!
    @IBOutlet var progressLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.contentView.backgroundColor = GrayColorInit
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        // 设置数据
        let url = URL(string: self.projectModel_Pro.listImageUrl)
        self.projectImageView .sd_setImage(with: url)
        
        self.moneyLabel.text = "¥"+String(describing: self.projectModel_Pro.amount)
        self.targetLabel.text = String(describing: self.projectModel_Pro.targetNum)
        self.progressLabel.text = String(describing: self.projectModel_Pro.userMaxOrder)+"%"
    }
    
}
