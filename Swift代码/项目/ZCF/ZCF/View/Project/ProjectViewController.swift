//
//  ProjectViewController.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/15.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ProjectViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - 变量
    
    // button按钮的数据
    var indexButton:Int!
    // cell里面的文字
    var regionArray1:Array<AnyObject>!
    var regionArray2:Array<Array<AnyObject>>!
    var typeArray:Array<AnyObject>!
    var stateArray:Array<AnyObject>!
    // 记录点击的是cell中的第几行
    var clickCellIndex:Int!
    
    // 中间cell的数据
    var projectModelArray:Array<AnyObject>!
    
    
    //MARK: - 控件
    @IBOutlet var regionTableView1: UITableView!
    @IBOutlet var regionTableView2: UITableView!
    
    @IBOutlet var typeAndStateTableView: UITableView!
    // 搜索框
    @IBOutlet var searchBar: UISearchBar!
    // 盛放button的视图
    @IBOutlet var buttonView: UIView!
    // 三个点击按钮
    @IBOutlet var regionButton: UIButton!
    @IBOutlet var typeButton: UIButton!
    @IBOutlet var stateButton: UIButton!
    // 显示选项的视图
    @IBOutlet var displayView: UIView!
    // 底部滑动视图
    @IBOutlet var overallTableView: UITableView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        self.view.backgroundColor = GrayColorInit
        
        // 初始化数据
        self._initDataList()
        // 创建视图
        self._initViews()
        // 网络请求
        self._requestDataList()
   
    }
    
    //MARK: -
    // 初始化数据
    func _initDataList()
    {
        self.indexButton = -1
        self.clickCellIndex = 0
        self.regionArray1 = ["全部地区" as AnyObject,"北京市" as AnyObject,"浙江省" as AnyObject]
        self.regionArray2 = [["" as AnyObject],["北京市" as AnyObject],["杭州市" as AnyObject,"绍兴市" as AnyObject]]
        self.typeArray = ["全部类型" as AnyObject,"小额众筹" as AnyObject,"海外众筹" as AnyObject,"度假型众筹" as AnyObject,"产品众筹" as AnyObject]
        self.stateArray = ["全部状态" as AnyObject,"预热期" as AnyObject,"众筹中" as AnyObject,"已结束" as AnyObject]
        
        self.projectModelArray = Array()
    }
    
    //创建视图  
    func _initViews()
    {
        
        // 设置搜索框
        self.searchBar.showsCancelButton = true
        for  sView:UIView in self.searchBar.subviews.last!.subviews
        {
            if sView.isKind(of: UIButton.self)
            {
                let  cancleBtn = sView as! UIButton
                cancleBtn .setTitle("搜索", for: UIControlState())
                cancleBtn.setTitleColor(UIColor.black, for: UIControlState())
                cancleBtn.titleLabel?.font = UIFont.systemFont(ofSize: 14)
            }
        }
        
        for i in 0  ..< 3 
        {
            let button = self.buttonView.viewWithTag(160+i) as! UIButton
            // 设置button中图片和文字的位置
            button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, -100)
            button.titleEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0)
            // 给button添加点击事件
            button.addTarget(self, action: #selector(ProjectViewController.btnAction(_:)), for: .touchUpInside)
        }
        
        
        // 设置tableView
        self.overallTableView.separatorStyle = .none
        self.regionTableView1.separatorStyle = .none
        self.regionTableView2.separatorStyle = .none
        self.typeAndStateTableView.separatorStyle = .none
        self.overallTableView.delegate = self
        self.overallTableView.dataSource = self
        self.regionTableView1.delegate = self
        self.regionTableView1.dataSource = self
        self.regionTableView2.delegate = self
        self.regionTableView2.dataSource = self
        self.typeAndStateTableView.delegate = self
        self.typeAndStateTableView.dataSource = self
        // 注册
        self.overallTableView.register(UINib(nibName: "ProjectTableViewCCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.regionTableView1.register(UINib(nibName: "DisPlayTCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.regionTableView2.register(UINib(nibName: "DisPlayTCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.typeAndStateTableView.register(UINib(nibName: "DisPlayTCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    //MARK: - 网络请求
    func _requestDataList()
    {
        let urlString = HttpUrl + "/project/list/condition"
        let request = HTTPRequest()
        let requestHead:[String:String] = ["version":"2.0","terminal":"ios"]
//        let dic:[String:AnyObject] = ["name":"","pageSize":"20"]
        
        request.POSTRequest(urlString, httpHeader: requestHead, parameters: nil, resultBlock: { (result) -> Void in
//            print("请求成功")
//            print(result)
            // 获取数据
            let data:[String:AnyObject] = result["data"] as! Dictionary
            let list:Array<AnyObject> = data["list"] as! Array
            for dic:AnyObject in list
            {
                let ProjectModel_Pro = ProjectListModel_Project(contentDic:dic as! [AnyHashable: Any])
                self.projectModelArray.append(ProjectModel_Pro!)
            }
            // 刷新数据
            self.overallTableView.reloadData()
            }) { () -> Void in
                print("请求失败")
        }
    }
    
    
    //MARK: - 点击事件
    
    // 选项button的点击事件
    func btnAction(_ sender:UIButton)
    {
        // 初始化点击button的位置
        self.clickCellIndex = 0
        
        // 已经点击开，并且第二次点击的于展开的button相同
        if self.indexButton == sender.tag - 160
        {
            sender.setImage(UIImage(named: "search_07"), for: UIControlState())
            self.displayView.isHidden = true
            self.indexButton = -1
            return
        }
        else
        {
            self.displayView.isHidden = false
            for i in 0  ..< 3 
            {
                let button:UIButton = self.buttonView.viewWithTag(160+i) as! UIButton
                button.setImage(UIImage(named: "search_07"), for: UIControlState())
            }
            sender.setImage(UIImage(named: "search_09"), for: UIControlState())
            self.indexButton = sender.tag - 160
        }
        
        // 设置视图的显隐
        if sender.tag == 160
        {
            self.typeAndStateTableView.isHidden = true
            self.regionTableView1.reloadData()
            self.regionTableView2.reloadData()
        }
        else if sender.tag == 161 || sender.tag == 162
        {
            self.typeAndStateTableView.isHidden = false
            self.typeAndStateTableView.reloadData()
        }
        
    }
    
    
    //MARK: - tableView的代理和数据源方法
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 点击的是全部地区的button
        if self.indexButton == 0
        {
            if tableView.tag == 201
            {
                return self.regionArray1.count
            }
            else if tableView.tag == 202
            {
                if self.clickCellIndex == 0
                {
                    return 0
                }
                return self.regionArray2[self.clickCellIndex].count
            }
            else
            {
                return self.projectModelArray.count
            }
        }
        // 点击的是众筹模式的button
        else if self.indexButton == 1
        {
            
            if tableView.tag == 203
            {
                return self.typeArray.count
            }
            else
            {
                return self.projectModelArray.count
            }
        }
        // 点击的是众筹状态
        else if self.indexButton == 2
        {
            if tableView.tag == 203
            {
                return self.stateArray.count
            }
            else
            {
                return self.projectModelArray.count
            }
        }
        else
        {
            return self.projectModelArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView.tag != 200)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DisPlayTCell
            
            // 设置表示图标
            if((indexPath as NSIndexPath).row == self.clickCellIndex)
            {
                cell.markerLineView.isHidden = false
            }
            else
            {
                cell.markerLineView.isHidden = true
            }
            
            // 设置单元格文本,三个不同的状态
            if tableView.tag == 201
            {
                
                cell.titleString = self.regionArray1[(indexPath as NSIndexPath).row] as! String
                // 刷新
                cell .setNeedsLayout()
            }
            if tableView.tag == 202
            {
                let array:Array<AnyObject> = self.regionArray2[self.clickCellIndex] 
                cell.titleString = array[(indexPath as NSIndexPath).row] as! String
                cell.markerLineView.isHidden = true
                cell.setNeedsLayout()
                
            }
            if tableView.tag == 203
            {
                if self.indexButton == 1
                {
                    cell.titleString = self.typeArray[(indexPath as NSIndexPath).row] as! String
                    cell.setNeedsLayout()
                }
                else
                {
                    cell.titleString = self.stateArray[(indexPath as NSIndexPath).row] as! String
                    cell.setNeedsLayout()
                }
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProjectTableViewCCell
            cell.projectModel_Pro = self.projectModelArray[(indexPath as NSIndexPath).row] as! ProjectListModel_Project
            // 刷新
            cell.setNeedsLayout()
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 201
        {
            self.clickCellIndex = (indexPath as NSIndexPath).row
            tableView .reloadData()
            self.regionTableView2.reloadData()
        }
        else if tableView.tag == 202 || tableView.tag == 203
        {
            self.clickCellIndex = (indexPath as NSIndexPath).row
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 200
        {
            return 51 + KScreenWidth/640*370
        }
        return 44
    }

}
