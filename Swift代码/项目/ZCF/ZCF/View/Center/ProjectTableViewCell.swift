//
//  ProjectTableViewCell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/22.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ProjectTableViewCell: UITableViewCell {
    
/// 变量
    
    // 图片的url地址
    var projectCellImageString:String!
    // 文Text文字n内容
    var textString:String!
    // 起购金额
    var startMoney:Int!
    // 目标份额
    var targetNumber:Int!
    // 年化收益率
    var numberString:String!
    
    
/// 控件
    
    // 主题图片试图
    @IBOutlet var projectCellImageView: UIImageView!
    // 文本内容
    @IBOutlet var titleLabel: UILabel!
    // 起购金额内容
    @IBOutlet var startMoneyLabel: UILabel!
    // 目标金额内容
    @IBOutlet var targetNumberLabel: UILabel!
    // 年化收益率内容
    @IBOutlet var numberStringLabel: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor ( red: 0.9046, green: 0.8999, blue: 0.9093, alpha: 1.0 )
    }
    
    override func layoutSubviews() {
        
        // 设置
        let url = URL(string: projectCellImageString)
        projectCellImageView .sd_setImage(with: url)
        titleLabel.text = textString
        startMoneyLabel.text = String(startMoney)
        targetNumberLabel.text = String(targetNumber)
//        // 数据变化
//        numberStringLabel.text = numberString
        numberStringLabel.text = ""
    }
    
}
