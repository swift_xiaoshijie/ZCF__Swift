//
//  MyselfTcell.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/18.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class MyselfTcell: UITableViewCell {
    
    //MARK: - 数据
    
    // 图片的名称
    var imageNameString:String!
    var titleString:String!
    
    
    //MARK: - 视图
    
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // 复用的方法
    override func layoutSubviews() {
        
        super.layoutSubviews()
        self.iconImageView.image = UIImage(named: self.imageNameString)
        self.titleLabel.text = self.titleString
    }
    
}
