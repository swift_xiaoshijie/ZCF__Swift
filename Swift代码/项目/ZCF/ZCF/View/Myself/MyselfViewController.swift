//
//  MyselfViewController.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/15.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class MyselfViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    //MARK: - 数据
    // 图标
    var imageDataListArray:Array<Array<AnyObject>>!
    // 文本
    var titleDataListArray:Array<Array<AnyObject>>!
    
    
    //MRAK: - 控件
    @IBOutlet var overallTableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 设置导航栏和状态栏
        self.navigationController?.navigationBar.isHidden = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        // 初始化数据
        self._initDataList()
        
        // 创建视图
        self._initViews()
        
    }
    
    // 初始化数据
    func _initDataList()
    {
        self.imageDataListArray = [["tb_03" as AnyObject,"tb_06" as AnyObject,"tb_08" as AnyObject],["tb_10" as AnyObject,"tb_12" as AnyObject,"tb_14" as AnyObject,"tb_06" as AnyObject],["tb_16" as AnyObject,"tb_20" as AnyObject,"tb_01" as AnyObject,"tb_02" as AnyObject]]
        self.titleDataListArray = [["我的订单" as AnyObject,"我的收藏" as AnyObject,"我的消息" as AnyObject],["积分商城" as AnyObject,"众筹钱包" as AnyObject,"个人设置" as AnyObject,"客户推荐" as AnyObject],["常见问题" as AnyObject,"新手指南" as AnyObject,"在线客服" as AnyObject,"系统设置" as AnyObject]]
    }

    // 创建视图
    func _initViews()
    {
        self.overallTableView.delegate = self
        self.overallTableView.dataSource = self
        
        let headView = Bundle.main.loadNibNamed("HeadView", owner: nil, options: nil)?.first as? HeadView
        self.overallTableView.tableHeaderView = headView
        
        // 注册单元格
        self.overallTableView .register(UINib(nibName: "MyselfTcell", bundle: nil), forCellReuseIdentifier: "cellID")
        
        
    }
    
    //mARK: - tableView的代理方法
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let array = self.imageDataListArray[section]
        return array.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.imageDataListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellID") as! MyselfTcell
        let imageArray:Array<AnyObject> = self.imageDataListArray[(indexPath as NSIndexPath).section] 
        let titleArray:Array<AnyObject> = self.titleDataListArray[(indexPath as NSIndexPath).section] 
        cell.imageNameString = imageArray[(indexPath as NSIndexPath).row] as! String
        cell.titleString = titleArray[(indexPath as NSIndexPath).row] as! String
        cell .setNeedsLayout()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
}
