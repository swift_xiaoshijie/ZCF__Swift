//
//  BaseViewController.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/28.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    
    //MARK: - 隐藏标签栏
    func setTabBarHidden()
    {
        let tabBar = self.tabBarController as! BaseTabBarController
        tabBar.tabBarImageView.isHidden = true
    }
    
    /**
      显示标签栏
     */
    func setTabBarNoHidden()
    {
        let tabBar = self.tabBarController as! BaseTabBarController
        tabBar.tabBarImageView.isHidden = false
    }

}
