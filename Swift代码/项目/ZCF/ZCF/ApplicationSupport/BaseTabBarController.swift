//
//  BaseTabBarController.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/14.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import Foundation
import UIKit






class BaseTabBarController:  UITabBarController{
    
    // 存放试图的数组
    var VCs : [UIViewController]!
    // 存放tabBar上面图片的数组(未点击)
    var tabBarClickOnImages: [String]!
    // 存放tabBar上面图片的数组(点击中)
    var tabBarClickUpImages: [String]!
    // 存放tabBar上面文字的数组
    var tabBarTitleString: [String]!
    
    
    // 全局的试图
    var tabBarImageView:UIImageView!
    
    override func viewDidLoad() {
        
        self.tabBar.isHidden = true
        
        
        // 初始化数据
        self._initDateList()
        
        // 创建结构
        self._createStructure()
        
        // 自定义标签栏
        self._customTabBar()
        
    }
    
    /**
     初始化数据
     */
    func _initDateList()
    {
        // 每个主界面试图
        let community = CommunityViewController()
        let mall = MallViewController()
        let center = CenterViewController()
        let project = ProjectViewController()
        let myself = MyselfViewController()
        VCs = [community,mall,center,project,myself]
        
        
        // 标签栏tabBar上面的图片(未点击)
        tabBarClickOnImages = ["sq_03","sq_05","zcfTabIcon","sousuo","wode"]
        // 标签栏tabBar上面的图片(点击中)
        tabBarClickUpImages = ["sq_18","sq_19","sq_16","sousuo_sel","wode_sel"]
        
        // 标签栏tabBar上面的文字
        tabBarTitleString = ["社区","商城","空","项目","我的"]
        
    }
    
    /**
     自定义标签栏
     */
    func _customTabBar()
    {
        self.tabBar.isHidden = true
        self.tabBarImageView = UIImageView(frame: CGRect(x: 0, y: KScreenHeight-44, width: KScreenWidth, height: 44))
        self.tabBarImageView.image = UIImage(named: "tabBarBGImg")
        self.tabBarImageView.isUserInteractionEnabled = true
        self.view.addSubview(self.tabBarImageView)
        
        for i in 0  ..< VCs.count
        {
            let width = 20+((KScreenWidth-150)/5.0+30)*CGFloat(i)
            let button = UIButton(type: .custom)
            if i == 2
            {
                button.frame = CGRect(x: width, y: 10, width: 36, height: 36)
                button.center = CGPoint(x: KScreenWidth/2.0, y: 44/2.0)
            }
            else
            {
                button.frame = CGRect(x: width, y: 10, width: 30, height: 30)
            }
            
            button .addTarget(self, action:#selector(BaseTabBarController.buttonAction(_:)), for: .touchUpInside)
            button .setImage(UIImage(named: tabBarClickOnImages[i]), for: UIControlState())
            button .setImage(UIImage(named: tabBarClickUpImages[i]), for: .selected)
            button.tag = 10 + i
            self.tabBarImageView.addSubview(button)
            
            if(i == 2)
            {
                button.isSelected = true
            }
            
        }
    }
    
    
    //MARK: - button的点击事件
    func buttonAction(_ sender: UIButton!)
    {
        for i in 0  ..< VCs.count 
        {
            let button = self.view.viewWithTag(i+10) as! UIButton
            button.isSelected = false

        }
        sender.isSelected = true
        self.selectedIndex = sender.tag - 10
    }
    
    /**
     创建结构
     */
    func _createStructure()
    {

        
        // 创建的时候也初始化
        var navigationVCs = Array<BaseNavigationController>()
        for i in 0  ..< VCs.count 
        {
            let navigationVC = BaseNavigationController(rootViewController:VCs[i])
            navigationVCs.append(navigationVC)
        }
        self.viewControllers = navigationVCs
        self.selectedIndex = 2
    }
}
