//
//  UIScrollView+ManyImage.m
//  Init
//
//  Created by 赵世杰 on 16/4/13.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "UIScrollView+ManyImage.h"
#import "TimerIndex.h"
#import "UIViewExt.h"
#import "UIImageView+WebCache.h"
#import <objc/runtime.h>

@implementation UIScrollView (ManyImage)

static char Timer ;
static char PageControl ;


#pragma mark - 设置属性的set，get方法----------------
- (void)setTimer:(NSTimer *)timer
{
    [self willChangeValueForKey:@"Timer"] ;
    objc_setAssociatedObject(self, &Timer, timer, OBJC_ASSOCIATION_ASSIGN) ;
    [self didChangeValueForKey:@"Timer"] ;
}

- (NSTimer *)timer
{
    return objc_getAssociatedObject(self, &Timer) ;
}

- (void)setPageControl:(UIPageControl *)pageControl
{
    [self willChangeValueForKey:@"PageControl"] ;
    objc_setAssociatedObject(self, &PageControl, pageControl, OBJC_ASSOCIATION_RETAIN_NONATOMIC) ;
    [self didChangeValueForKey:@"PageControl"] ;
}

- (UIPageControl *)pageControl
{
    return objc_getAssociatedObject(self, &PageControl) ;
}




#pragma mark - 方法实现-----------------
- (void)addImageToScrollViewWithImagesArray:(NSArray *)array
{
    
    self.pagingEnabled = YES ;
    self.showsHorizontalScrollIndicator = NO ;
    
    // 设置数据
    TimerIndex *timerIndex = [TimerIndex shareTimerIndex] ;
    [timerIndex.imageArray addObjectsFromArray:array] ;
    [timerIndex.imageArray insertObject:array.lastObject atIndex:0] ;
    [timerIndex.imageArray addObject:array.firstObject] ;
    
    self.contentSize = CGSizeMake(timerIndex.imageArray.count*self.width, self.height) ;
    self.delegate = self ;
    [self setContentOffset:CGPointMake(self.width, 0)] ;
    
    for(int i = 0 ; i < timerIndex.imageArray.count ; i++)
    {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(i*self.width, 0, self.width, self.height)] ;
        NSURL *url = [NSURL URLWithString:timerIndex.imageArray[i]] ;
        [imageView sd_setImageWithURL:url] ;
        [self addSubview:imageView] ;
    }
    
    
    self.pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(self.left+10, self.bottom-20, self.width-20, 20)] ;
    
    self.pageControl.numberOfPages = array.count ;
    [self.pageControl addTarget:self action:@selector(pageControlAction:) forControlEvents:UIControlEventTouchDragInside] ;
    // 获取scrollView的父视图并且判断
    if([self.superview isKindOfClass:[UIView class]])
    {
        [self.superview addSubview:self.pageControl] ;
    }
    else if([self.superview isKindOfClass:[UIViewController class]])
    {
        __weak UIViewController *superViewVC = (UIViewController *)self.superview ;
        [superViewVC.view addSubview:self.pageControl] ;
    }
    
    // 设置定时器
    [self.timer invalidate] ;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(timerAction) userInfo:nil repeats:YES] ;
        [[NSRunLoop currentRunLoop]run] ;
    });

    
}


// 定时器调用方法
- (void)timerAction
{
    TimerIndex *timerIndex = [TimerIndex shareTimerIndex] ;
    timerIndex.index ++ ;
    self.userInteractionEnabled = NO ;
    [UIView animateWithDuration:1.2 animations:^{
        [self setContentOffset:CGPointMake(self.width*timerIndex.index, 0)] ;
    } completion:^(BOOL finished) {
        self.userInteractionEnabled = YES ;
        
        if(timerIndex.index >= timerIndex.imageArray.count-1)
        {
            [self setContentOffset:CGPointMake(self.width, 0)] ;
            timerIndex.index = 1 ;
            
        }
        self.pageControl.currentPage = timerIndex.index-1 ;
    }];
    
    
}

#pragma mark - pageControl的点击事件
- (void)pageControlAction:(UIPageControl *)pageControl
{
    [self setContentOffset:CGPointMake((pageControl.currentPage+1)*self.width, 0)] ;
}

#pragma ScrollView 的代理方法
// 手指接触视图开始拖拽的时候调用的协议方法
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    // 移除
    [self.timer invalidate] ;
}

// 手指离开时调用的方法
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    // 开启
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(timerAction) userInfo:nil repeats:YES] ;
        [[NSRunLoop currentRunLoop]run] ;
    });
    self.userInteractionEnabled = NO ;
}

// 减速结束
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self scrollDidSroll] ;
    self.userInteractionEnabled = YES ;
}



#pragma mark - 当滑动视图停止滑动的时候执行一些操作
// 当滑动视图停止滑动的时候执行一些操作
- (void)scrollDidSroll
{
    TimerIndex *timerIndex = [TimerIndex shareTimerIndex] ;
    int pageIndex = (int)self.contentOffset.x/self.width ;
    if(pageIndex <= 0)
    {
        pageIndex = (int)timerIndex.imageArray.count-2 ;
        [self setContentOffset:CGPointMake(pageIndex*self.width, 0)] ;
    }
    else if(pageIndex >= timerIndex.imageArray.count-1)
    {
        pageIndex = 1 ;
        [self setContentOffset:CGPointMake(self.width, 0)] ;
    }
    self.pageControl.currentPage = pageIndex-1 ;

    timerIndex.index = pageIndex ;
}



@end
