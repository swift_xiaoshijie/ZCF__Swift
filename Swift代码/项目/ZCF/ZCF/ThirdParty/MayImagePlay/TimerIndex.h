//
//  TimerIndex.h
//  Init
//
//  Created by 赵世杰 on 16/4/13.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerIndex : NSObject


// 所有的图片加上前后两张
@property (nonatomic,strong) NSMutableArray *imageArray ;


// 第几张
@property (nonatomic,assign) NSInteger index ;

+ (TimerIndex *)shareTimerIndex ;

@end
