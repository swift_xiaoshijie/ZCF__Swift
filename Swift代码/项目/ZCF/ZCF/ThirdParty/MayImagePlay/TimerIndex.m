//
//  TimerIndex.m
//  Init
//
//  Created by 赵世杰 on 16/4/13.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "TimerIndex.h"

@implementation TimerIndex

+ (TimerIndex *)shareTimerIndex
{
    static TimerIndex *timerIndex = nil ;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        timerIndex = [[TimerIndex alloc]init] ;
        timerIndex.index = 1 ;
        timerIndex.imageArray = [[NSMutableArray alloc]init] ;
    });
    return timerIndex ;
}

@end
