//
//  UIScrollView+ManyImage.h
//  Init
//
//  Created by 赵世杰 on 16/4/13.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (ManyImage)<UIScrollViewDelegate>

#pragma mark - 变量-------------------------------------------------------------

@property (nonatomic,strong)NSTimer *timer ;


#pragma mark - 视图-------------------------------------------------------------
@property (nonatomic,strong)UIPageControl *pageControl ;


#pragma mark - 方法-------------------------------------------------------------
- (void)addImageToScrollViewWithImagesArray:(NSArray *)array ;

@end
