//
//  CBIndicatorView.m
//  ZCF_2.0
//
//  Created by chebao on 15/11/13.
//  Copyright © 2015年 chebao. All rights reserved.
//

#import "CBIndicatorView.h"

@implementation CBIndicatorView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _indicator.center = self.center;
        _indicator.bounds = CGRectMake(0, 0, 60, 60);
        _indicator.backgroundColor = [UIColor blackColor];
        _indicator.alpha = 0.5;
        _indicator.hidesWhenStopped = YES;
        _indicator.layer.cornerRadius = 6;
        _indicator.layer.masksToBounds = YES;
        [_indicator startAnimating];
        
        [self addSubview:_indicator];
        
    }
    return self;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
