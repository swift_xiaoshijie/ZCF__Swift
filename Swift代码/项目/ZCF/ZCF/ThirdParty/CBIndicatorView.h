//
//  CBIndicatorView.h
//  ZCF_2.0
//
//  Created by chebao on 15/11/13.
//  Copyright © 2015年 chebao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBIndicatorView : UIView
@property (strong, nonatomic) UIActivityIndicatorView * indicator;



@end
