//
//  UserSingle.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/19.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class UserSingle: NSObject {
    
    /*  老的单利方法
    private static var __once: () = { () -> Void in
            UserSingle.instance = UserSingle()
        }()
    
    // 数据变量
    var token:String!
    var userId:String!
    
    class func shareInstance() -> UserSingle {
        struct SJSingle{
            static var userSingle:Int = 0
            static var instance:UserSingle? = nil
        }
        _ = UserSingle.__once
        return SJSingle.instance!
    }
 */
    internal static let instance = UserSingle()
    // 数据变量
    var token:String!
    var userId:String!
    //必须保证init方法的私有性，只有这样，才能保证单例是真正唯一的，避免外部对象通过访问init方法创建单例类的其他实例。由于Swift中的所有对象都是由公共的初始化方法创建的，我们需要重写自己的init方法，并设置其为私有的。
    private override init(){
        super.init() ;
        print("create 单例")
    }
    
}
