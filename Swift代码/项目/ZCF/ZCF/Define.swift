//
//  Define.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/18.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit


// 屏幕的大小
let KScreenWidth = UIScreen.main.bounds.size.width
let KScreenHeight = UIScreen.main.bounds.size.height

 /// 网络请求前面一部分

// 真实版本
let HttpUrl:String = "http://service.bdzcf.com/zcfservice"
// 测试版本
//let HttpUrl:String = "http://192.168.11.103:8080/zcfservice"


// 请求的信息
//let userId:String = "30669"
//let token:String = "4D84BF2CA9E2480BAD030F2DC84550EF"


// 适配不同屏幕大小，实现控件的等比例缩放布局
let scale = Method.Scale()


// 背景的浅灰色
let GrayColorInit = UIColor ( red: 0.9137, green: 0.9042, blue: 0.9232, alpha: 1.0 )
