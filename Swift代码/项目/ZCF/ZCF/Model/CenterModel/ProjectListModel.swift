//
//  ProjectListModel.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/21.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ProjectListModel{
    // 参数
    var aggregateAmount:Int!
    var amount:Int!
    var chipsAmount:Int!
    var collectStatus:Int!
    var id:Int!
    var label:String!
    var listImageUrl:String!
    var name:String!
    var progress:Int!
    var remainDays:Int!
    var startRemainDays:Int!
    var supportNum:Int!
    var targetNum:Int!
    var typeId:Int!
    var userMaxOrder:Int!
    var yield:String!
    func initWithDictionary(dic:AnyObject)
    {

        aggregateAmount = dic["aggregateAmount"] as! Int
        amount          = dic["amount"] as! Int
        chipsAmount     = dic["chipsAmount"] as! Int
        collectStatus   = dic["collectStatus"] as! Int
        id              = dic["id"] as! Int
        label           = dic["label"] as! String
        listImageUrl    = dic["listImageUrl"] as! String
        name            = dic["name"] as! String
        progress        = dic["progress"] as! Int
        remainDays      = dic["remainDays"] as! Int
        startRemainDays = dic["startRemainDays"] as! Int
        supportNum      = dic["supportNum"] as! Int
        targetNum       = dic["targetNum"] as! Int
        typeId          = dic["typeId"] as! Int
        userMaxOrder    = dic["userMaxOrder"] as! Int
        yield           = dic["yield"] as! String
        
    }


}


