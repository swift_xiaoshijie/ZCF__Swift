//
//  HeadScrollerModel.swift
//  ZCF
//
//  Created by 赵世杰 on 16/3/21.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class HeadScrollerModel {
    
    var id:Int!
    var order:Int!
    var url:String!
    
    
    func initWithDictionary(dic:AnyObject)
    {
        id = dic["id"] as! Int
        order = dic["order"] as! Int
        url = dic["url"] as! String
    }

}
