//
//  ItemShow_Mall.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/14.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ItemShow_Mall: BaseModel {

    var exchangeNum:NSNumber!
    var goodsImageList:Array<AnyObject>!
    var id:NSNumber!
    var name:String!
    var score:NSNumber!
    var views:NSNumber!
}
