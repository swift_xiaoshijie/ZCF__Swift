//
//  ProjectListModel_Project.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/15.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class ProjectListModel_Project: BaseModel {

    var aggregateAmount:NSNumber!
    var amount:NSNumber!
    var chipsAmount:NSNumber!
    var collectStatus:NSNumber!
    var id:NSNumber!
    var label:String!
    var listImageUrl:String!
    var name:String!
    var progress:NSNumber!
    var remainDays:NSNumber!
    var startRemainDays:NSNumber!
    var supportNum:NSNumber!
    var targetNum:NSNumber!
    var typeId:NSNumber!
    var userMaxOrder:NSNumber!
    var yield:String!
}
