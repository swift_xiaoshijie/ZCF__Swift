//
//  HeadScrllerModel_Community.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/8.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import Foundation

class HeadScrllerModel_Community {
    
    var id:Int!
    var order:Int!
    var url:String!
    
    func initWithDictionary(dic:AnyObject)
    {
        id = dic["id"] as! Int
        order = dic["order"] as! Int
        url = dic["url"] as! String
        
    }
    
}
