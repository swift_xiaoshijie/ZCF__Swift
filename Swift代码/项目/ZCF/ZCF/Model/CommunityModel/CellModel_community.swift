//
//  CellModel_community.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/11.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import UIKit

class CellModel_community: BaseModel {
    
    var content:String!
    var createTime:String!
    var id:Int!
    var imageList:Array<AnyObject>!
    var likes:Int!
    var position:String!
    var replyNum:Int!
    var stick:Int!
    var terminal:String!
    var title:String!
    var userHeaderImg:String!
    var userLikeStatus:Int!
    var userNickname:String!
    var views:Int!
    
}
