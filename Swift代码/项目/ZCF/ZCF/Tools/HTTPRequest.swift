//
//  HTTPRequest.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/7.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import Foundation
import UIKit

class HTTPRequest: NSObject {
    
    
    
    /**
     网络请求POST的二次封装
     
     - parameter urlString:   网络URL地址
     - parameter httpHeader:  请求头内容
     - parameter parameters:  请求体参数
     - parameter resultBlock: 返回结果
     - parameter errorBlock:  返回错误信息
     */
    func POSTRequest(_ urlString:String , httpHeader:[String:String]? = nil, parameters:[String:String]? = nil,resultBlock:@escaping (_ result:(AnyObject))->Void,errorBlock:@escaping ()->Void)
    {
        // 创建对象
        let manager:AFHTTPSessionManager = AFHTTPSessionManager()
        // 判断不为空进行遍历
        if(httpHeader != nil)
        {
            for( HTTPHeaderKey , HTTPHeaderValue ) in httpHeader!
            {
                manager.requestSerializer.setValue(HTTPHeaderValue, forHTTPHeaderField: HTTPHeaderKey)
            }
        }
        if(parameters != nil)
        {
            for( paramKey , paramValue ) in parameters!
            {
 
                manager.requestSerializer.setValue(paramValue, forHTTPHeaderField: paramKey)
            }
        }
        
//        manager .post(urlString, parameters: parameters, success: { (requestOperation:AFHTTPRequestOperation!, resultObject:AnyObject!) -> Void in
//            resultBlock(resultObject)
//            }) { (requestOperation:AFHTTPRequestOperation!, error:NSError!) -> Void in
//                errorBlock()
//        }
        manager.post(urlString, parameters: parameters, progress: nil, success: { (task:URLSessionDataTask, resultObject:Any?) in
            resultBlock(resultObject! as (AnyObject))
        }) { (task:URLSessionDataTask?, error:Error?) in
            errorBlock()
        }
        

    }
    
    
}


