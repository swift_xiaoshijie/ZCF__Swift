//
//  RequestTool.h
//  ZCF
//
//  Created by 赵世杰 on 16/5/3.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestTool : NSObject


/**
 *  网络请求返回的参数
 *
 *  @param code    参数
 *  @param message 未知错误
 *
 *  @return 请求的结果
 */
+ (NSString *)messgeDealWithCode:(NSString *)code message:(NSString *)message ;


//传入任何类型  返回string
+ (NSString *)sjGetString:(id)unKnowObj ;

@end
