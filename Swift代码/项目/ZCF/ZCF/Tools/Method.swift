//
//  Method.swift
//  ZCF
//
//  Created by 赵世杰 on 16/4/11.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

import Foundation


class Method {
    
    
    /**
     判断机型，获取比例
     
     - returns: 比例值
     */
    static func Scale() -> CGFloat
    {
        var scale:CGFloat?
        if KScreenWidth == 320
        {
            scale = 1
        }
        else if KScreenWidth == 375
        {
            scale = 375/320.0
        }
        else if KScreenWidth == 414
        {
            scale = 414/320.0
        }
        return scale!
    }
    
    
    /**
     转换大小位置
     
     - parameter frame: 5s的位置大小
     
     - returns: 缩放之后的大小
     */
    static func ScaleFrame(_ frame:CGRect) -> CGRect
    {
        let point_X = frame.origin.x * scale
        let point_Y = frame.origin.y * scale
        let width = frame.size.width * scale
        let height = frame.size.height * scale
        return CGRect(x: point_X, y: point_Y, width: width, height: height)
    }
}
