//
//  RequestTool.m
//  ZCF
//
//  Created by 赵世杰 on 16/5/3.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "RequestTool.h"

@implementation RequestTool



/**
 *  网络请求返回的参数
 *
 *  @param code    参数
 *  @param message 未知错误
 *
 *  @return 请求的结果
 */
+ (NSString *)messgeDealWithCode:(NSString *)code message:(NSString *)message
{
    NSLog(@"code:%@",code);
    if ([code isEqualToString:@"202"]) {
        return @"网络请求失败";
    }
    else if ([code isEqualToString:@"20100"]){
        return @"登录超时";
    }
    else if ([code isEqualToString:@"20101"]){
        return @"账号不存在";
    }
    else if ([code isEqualToString:@"20102"]){
        return @"账号或密码错误";
    }
    else if ([code isEqualToString:@"20107"]){
        return @"验证码错误";
    }
    else if ([code isEqualToString:@"20112"]){
//        [UserInfo getUserInfor].isLogin = NO;
        return @"异地登录，请重新登录";
    }
    else if ([code isEqualToString:@"20113"]){
        return @"手机号码已注册";
    }
    else if ([code isEqualToString:@"20114"]){
        return message;
    }
    else if ([code isEqualToString:@"20115"]){
        return @"图形验证码错误";
    }
    else if ([code isEqualToString:@"20116"]){
        return @"手机验证码错误";
    }
    else if ([code isEqualToString:@"20117"]){
        return message;
    }
    else if ([code isEqualToString:@"21101"]){
        return @"缺少请求参数，或者参数错误";
    }
    else if ([code isEqualToString:@"20126"]){
        return @"已签到";
    }
    else if ([code isEqualToString:@"20128"]){
        return message;
    }
    else if ([code isEqualToString:@"20129"]){
        return @"原始密码错误";
    }
    else{
        return @"网络错误";
    }
}


/**
 *  返回字符串
 *
 *  @param UnknowObj 传入的数据类型
 *
 *  @return 返回的字符串
 */
+ (NSString *)sjGetString:(id)unKnowObj
{
    if((NSObject*)unKnowObj==[NSNull null] || unKnowObj == nil)
        return @"";
    if([unKnowObj isKindOfClass:[NSString class]])
    {
        return unKnowObj;
    }
    else if ([unKnowObj isKindOfClass:[NSNumber class]])
    {
        return [unKnowObj stringValue];
    }
    
    return nil;
}

@end
